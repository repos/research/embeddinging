import socket

import skein
import typer

app = typer.Typer()


def get_skein_client() -> skein.ApplicationClient:
    return skein.ApplicationClient.from_current()


@app.command()
def persist_host() -> None:
    host = socket.gethostbyname(socket.gethostname())

    # Form a unique key to store the address using the current container id
    key = f"address.{skein.properties.container_id}"

    # The key-value store only accepts bytes as values
    value = host.encode()

    # Store the server address in the key-value store, assigning the current
    # container as the owner of the key. This ensures that the key is deleted if
    # the container exits.
    client = get_skein_client()
    client.kv.put(key, value, owner=skein.properties.container_id)


@app.callback()
def main() -> None:
    """Miscellaneous hooks for Skein apps."""


if __name__ == "__main__":
    app()
