import gzip
import json
import logging
from enum import StrEnum, unique
from pathlib import Path
from typing import Any, Self, TypeAlias, assert_never

import rich
import typer
from pydantic import AnyHttpUrl, Field, RootModel
from pydantic.dataclasses import dataclass
from tqdm import tqdm

from benchmarks.client import Client, InsertStats, SearchStats
from benchmarks.clients.annoy import AnnoyClient
from benchmarks.clients.qdrant import QdrantClient
from benchmarks.reader import read_queries, read_records

app = typer.Typer()


@unique
class ClientKind(StrEnum):
    ANNOY = "annoy"
    QDRANT = "qdrant"


Params: TypeAlias = dict[str, Any]


@dataclass(frozen=True)
class ExperimentConfig:
    name: str
    client: ClientKind
    connection_params: Params = Field(default_factory=lambda: {})
    collection_params: Params = Field(default_factory=lambda: {})
    search_params: list[Params] = Field(default_factory=lambda: [{}])
    insert_params: Params = Field(default_factory=lambda: {})

    @classmethod
    def load(cls, experiment_name: str) -> Self:
        config_dir = Path(__file__).resolve().parent / "experiments"
        config_path = config_dir / f"{experiment_name}.json"
        with open(config_path) as f:
            return cls(**json.load(f))


@dataclass(frozen=True)
class ExperimentResult:
    name: str
    search_stats: dict[str, SearchStats]
    insert_stats: dict[str, InsertStats]

    def to_json(self, output_path: Path) -> None:
        with open(output_path, "w") as file:
            serializable_result = RootModel[ExperimentResult](self)
            file.write(serializable_result.model_dump_json())


def get_client(server: AnyHttpUrl, config: ExperimentConfig) -> Client:
    match config.client:
        case ClientKind.ANNOY:
            return AnnoyClient(
                vector_size=50,
                index_file_name=Path("annoy_benchmark.ann"),
            )

        case ClientKind.QDRANT:
            return QdrantClient(
                vector_size=50,
                connection_params=(
                    # Override the server url in the config
                    # with the one passed through cli.
                    config.connection_params
                    | {"url": str(server)}
                ),
                collection_params=config.collection_params,
            )

        case other:
            assert_never(other)


def make_params_identifier(params: Params) -> str:
    if not params:
        raise ValueError(
            "Params should have at least one key value "
            "pair to make a valid identifier"
        )
    kv_strs = [f"{key}-{value}" for key, value in params.items()]
    return "-".join(kv_strs)


def run(
    config: ExperimentConfig, server: AnyHttpUrl, dataset: Path
) -> ExperimentResult:
    records_path = dataset / "embeddings.tsv.gz"
    queries_path = dataset / "queries.jsonl"

    client = get_client(server, config)

    with gzip.open(records_path, "rt") as record_file:
        records = read_records(record_file)
        insert_stats_id = make_params_identifier(config.insert_params)
        insert_stats = {
            insert_stats_id: client.insert(
                records=records,
                insert_params=config.insert_params,
            )
        }

    with open(queries_path) as query_file:
        all_search_stats = {}
        for search_params in tqdm(config.search_params):
            # seek to the beginning of records and queries
            # since they are read in as iterators and can only
            # be consumed once.
            query_file.seek(0)
            queries = read_queries(query_file)

            search_stats_id = make_params_identifier(search_params)
            search_stats = client.search(queries, search_params=search_params)
            all_search_stats[search_stats_id] = search_stats

    return ExperimentResult(
        name=config.name,
        search_stats=all_search_stats,
        insert_stats=insert_stats,
    )


@app.command()
def main(
    name: str = typer.Option(
        help="Name of the experiment to be run for benchmarking",
    ),
    dataset: Path = typer.Option(
        help="Path to dataset to be used for the experiment",
    ),
    output: Path = typer.Option(
        help="Path to json file to write the experiment results to",
    ),
    server: str = typer.Option(
        help="Url for the vector database to use for experiments",
        default="http://localhost",
    ),
) -> None:
    logging.basicConfig(level=logging.INFO)

    config = ExperimentConfig.load(experiment_name=name)
    result = run(
        config=config,
        server=AnyHttpUrl(server),
        dataset=dataset,
    )

    rich.print(f"name: [bold green]{result.name}[/bold green]")
    rich.print(result.insert_stats)
    rich.print(result.search_stats)

    result.to_json(output_path=output)


if __name__ == "__main__":
    app()
