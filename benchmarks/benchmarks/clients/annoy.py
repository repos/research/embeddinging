import logging
import time
from collections.abc import Iterable
from concurrent.futures import ProcessPoolExecutor
from dataclasses import dataclass
from datetime import timedelta
from itertools import chain, repeat
from pathlib import Path
from typing import Any

import numpy as np
from annoy import AnnoyIndex

from benchmarks.client import InsertStats, SearchStats
from benchmarks.reader import Query, Record
from benchmarks.util import batched


@dataclass(frozen=True)
class _NearestNeighborSearchStats:
    time: timedelta
    precision: float


class AnnoyClient:
    def __init__(
        self,
        vector_size: int,
        index_file_name: Path,
        n_trees: int = 25,
    ) -> None:
        self.vector_size = vector_size
        self.n_trees = n_trees
        self.index_file_name = index_file_name

    def insert(
        self, records: Iterable[Record], insert_params: dict[str, Any]
    ) -> InsertStats:
        #  prepare annoy to build the index in the specified file
        #  instead of in memory.
        index = AnnoyIndex(self.vector_size, metric="angular")
        index.on_disk_build(fn=str(self.index_file_name))
        index.set_seed(42)

        logging.info("Inserting records...")
        start = time.perf_counter()
        for record in records:
            index.add_item(i=record.id, vector=record.vector)
        insert_time = time.perf_counter() - start

        logging.info("Building index...")
        start = time.perf_counter()
        index.build(n_trees=self.n_trees, n_jobs=insert_params.get("parallel", 1))
        processing_time = time.perf_counter() - start

        return InsertStats(
            insert_time=timedelta(seconds=insert_time),
            processing_time=timedelta(seconds=processing_time),
            total_time=timedelta(seconds=insert_time + processing_time),
        )

    def _get_nearest_neighbors(
        self, queries: Iterable[Query], search_k: int
    ) -> list[_NearestNeighborSearchStats]:
        results = []
        index = AnnoyIndex(self.vector_size, metric="angular")
        index.load(fn=str(self.index_file_name))

        for query in queries:
            top_n = len(query.expected_results)
            start = time.perf_counter()
            ids = index.get_nns_by_vector(
                vector=query.vector, n=top_n, search_k=search_k
            )
            total_time = time.perf_counter() - start
            precision = len(set(query.expected_results).intersection(ids)) / top_n

            results.append(
                _NearestNeighborSearchStats(
                    time=timedelta(seconds=total_time),
                    precision=precision,
                )
            )

        return results

    def search(
        self,
        queries: Iterable[Query],
        search_params: dict[str, Any],
    ) -> SearchStats:
        parallel = search_params.get("parallel", 1)

        logging.info("Running queries...")

        with ProcessPoolExecutor(max_workers=parallel) as executor:
            start = time.perf_counter()
            results = list(
                executor.map(
                    self._get_nearest_neighbors,
                    batched(queries, batch_size=1024),
                    repeat(search_params["k"]),
                    timeout=30,
                )
            )

        total_time = time.perf_counter() - start
        all_results = list(chain(*results))
        latencies = [result.time.total_seconds() for result in all_results]
        precisions = [result.precision for result in all_results]

        return SearchStats(
            min_time=timedelta(seconds=np.min(latencies)),
            max_time=timedelta(seconds=np.max(latencies)),
            mean_time=timedelta(seconds=np.mean(latencies, dtype=float)),
            p95_time=timedelta(seconds=float(np.percentile(latencies, 95))),
            p99_time=timedelta(seconds=float(np.percentile(latencies, 99))),
            std_time=timedelta(seconds=np.std(latencies, dtype=float)),
            total_time=timedelta(seconds=total_time),
            requests_per_second=len(latencies) / total_time,
            mean_precision=np.mean(precisions, dtype=float),
        )
