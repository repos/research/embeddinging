import logging
import multiprocessing as mp
import time
from collections.abc import Iterable
from concurrent.futures import ProcessPoolExecutor
from dataclasses import dataclass
from datetime import timedelta
from functools import cache
from itertools import repeat
from typing import Any

import httpx
import numpy as np
import qdrant_client
from qdrant_client.http import models as qdrant_models

from benchmarks.client import InsertStats, SearchStats
from benchmarks.reader import Query, Record


@cache
def _get_qdrant_remote_client(**kwargs: Any) -> qdrant_client.QdrantClient:
    return qdrant_client.QdrantClient(
        limits=httpx.Limits(
            max_connections=None,
            max_keepalive_connections=0,
        ),
        **kwargs,
    )


@dataclass(frozen=True)
class _SearchOneStats:
    time: timedelta
    precision: float


class QdrantClient:
    def __init__(
        self,
        vector_size: int,
        connection_params: dict[str, Any],
        collection_params: dict[str, Any] = {},
    ) -> None:
        self.collection_name = "benchmark"
        self.connection_params = connection_params
        self.collection_params = collection_params
        self.vector_size = vector_size

        self._client.recreate_collection(
            collection_name=self.collection_name,
            vectors_config=qdrant_models.VectorParams(
                size=vector_size,
                distance=qdrant_models.Distance.COSINE,
            ),
            **self.collection_params,
        )

        self._client.update_collection(
            collection_name=self.collection_name,
            optimizer_config=qdrant_models.OptimizersConfigDiff(
                max_optimization_threads=0,
            ),
        )

    @property
    def _client(self) -> qdrant_client.QdrantClient:
        return _get_qdrant_remote_client(
            prefer_grpc=True,
            **self.connection_params,
        )

    def optimize(self) -> timedelta:
        logging.info("Optimizing collection...")
        self._client.update_collection(
            collection_name=self.collection_name,
            optimizer_config=qdrant_models.OptimizersConfigDiff(
                max_optimization_threads=1,
            ),
        )
        start = time.perf_counter()

        # wait for the collection to be green i.e.
        # all points fully processed and indexed.
        while True:
            time.sleep(5.0)
            collection_info = self._client.get_collection(self.collection_name)
            if collection_info.status != qdrant_models.CollectionStatus.GREEN:
                continue

            # Sometimes collections are momentarily reported as green even
            # though they are not done optimizing so we need to wait some more
            # to make sure the collections stays green.
            time.sleep(5.0)
            collection_info = self._client.get_collection(self.collection_name)
            if collection_info.status == qdrant_models.CollectionStatus.GREEN:
                break

        return timedelta(seconds=time.perf_counter() - start)

    def insert(
        self,
        records: Iterable[Record],
        insert_params: dict[str, Any],
    ) -> InsertStats:
        start = time.perf_counter()

        collection_records = (
            qdrant_models.Record(id=record.id, vector=record.vector)
            for record in records
        )

        logging.info("Uploading records...")
        self._client.upload_records(
            collection_name=self.collection_name,
            records=collection_records,
            **insert_params,
        )

        insert_time = time.perf_counter() - start
        processing_time = self.optimize().total_seconds()

        return InsertStats(
            insert_time=timedelta(seconds=insert_time),
            processing_time=timedelta(seconds=processing_time),
            total_time=timedelta(seconds=insert_time + processing_time),
        )

    def _search_one(
        self, query: Query, search_params: dict[str, Any]
    ) -> _SearchOneStats:
        max_retries = 5
        top_n = len(query.expected_results)
        start = time.perf_counter()

        while (retries := 0) < max_retries:
            try:
                attempt_start = time.perf_counter()
                results = self._client.search(
                    collection_name=self.collection_name,
                    query_vector=query.vector,
                    limit=top_n,
                    search_params=qdrant_models.SearchParams(**search_params),
                )
                attempt_time = time.perf_counter() - attempt_start
                ids = set(point.id for point in results)
                precision = len(set(query.expected_results).intersection(ids)) / top_n

                return _SearchOneStats(
                    time=timedelta(seconds=attempt_time),
                    precision=precision,
                )

            except Exception as err:
                retries += 1
                logging.exception(err)
                logging.info(f"Retrying: ({retries} / {max_retries})")

        return _SearchOneStats(
            time=timedelta(seconds=time.perf_counter() - start),
            precision=0,
        )

    def search(
        self,
        queries: Iterable[Query],
        search_params: dict[str, Any],
    ) -> SearchStats:
        parallel = search_params.pop("parallel", 1)

        # Since grpc uses multithreading, we don't want
        # to use fork as the start method to avoid deadlocks
        # in child processes.
        all_start_methods = mp.get_all_start_methods()
        start_method = "forkserver" if "forkserver" in all_start_methods else "spawn"

        logging.info("Running search queries...")
        with ProcessPoolExecutor(
            mp_context=mp.get_context(method=start_method),
            max_workers=parallel,
        ) as executor:
            start = time.perf_counter()
            results = list(
                executor.map(
                    self._search_one,
                    queries,
                    repeat(search_params),
                    timeout=15 * 60,
                    chunksize=1024,
                )
            )

        total_time = time.perf_counter() - start
        latencies = [result.time.total_seconds() for result in results]
        precisions = [result.precision for result in results]

        return SearchStats(
            min_time=timedelta(seconds=np.min(latencies)),
            max_time=timedelta(seconds=np.max(latencies)),
            mean_time=timedelta(seconds=np.mean(latencies, dtype=float)),
            p95_time=timedelta(seconds=float(np.percentile(latencies, 95))),
            p99_time=timedelta(seconds=float(np.percentile(latencies, 99))),
            std_time=timedelta(seconds=np.std(latencies, dtype=float)),
            total_time=timedelta(seconds=total_time),
            requests_per_second=len(latencies) / total_time,
            mean_precision=np.mean(precisions, dtype=float),
        )
