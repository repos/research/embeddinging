import logging
import sys
from pathlib import Path

import rich
import skein
import typer

app = typer.Typer()


@app.command()
def launch(spec_file: Path) -> None:
    """Launch a server as a Skein application."""
    spec = skein.ApplicationSpec.from_file(spec_file)
    hooks_path = Path(__file__).resolve().parent / "skein" / "hooks.py"
    hooks_file = skein.File(source=str(hooks_path))
    for service in spec.services.values():
        service.files["hooks.py"] = hooks_file
        commands = service.script.splitlines()
        env_cmd_idx = next(
            (
                idx
                for idx, cmd in enumerate(commands)
                if cmd.startswith("source") and cmd.endswith("/activate")
            ),
            None,
        )
        if env_cmd_idx is None:
            logging.error(
                "Couldn't find command for activating conda env in script. "
                "Are you sure you're shipping and activating one?"
            )
            sys.exit(1)

        service.script = "\n".join(
            commands[: env_cmd_idx + 1]
            + ["python3 hooks.py persist-host"]
            + commands[env_cmd_idx + 1 :]
        )

    with skein.Client() as client:
        app_id = client.submit(spec)
        rich.print(f"[bold green]Skein application ID:[/bold green] {app_id}")


@app.command()
def hosts(app_id: str) -> None:
    """Get IP addresses for all service instances running
    as part of the server app.
    """
    app = skein.Client().connect(app_id)
    for key, address in app.kv.get_prefix("address.").items():
        decoded_address = address.decode()
        rich.print(f"[bold green]{key}:[/bold green] {decoded_address}")


@app.command()
def status(app_id: str) -> None:
    """Get status of the server Skein app."""
    with skein.Client() as client:
        report = client.application_report(app_id)
        for property in ("id", "name", "state", "final_status"):
            rich.print(f"[bold green]{property}:[/bold green]", end="\t")
            rich.print(f"{getattr(report, property)}")


@app.command()
def kill(app_id: str) -> None:
    """Kill the server skein app."""
    with skein.Client() as client:
        client.kill_application(app_id)


@app.callback()
def main() -> None:
    """Manage a server running as a Skein application."""


if __name__ == "__main__":
    app()
