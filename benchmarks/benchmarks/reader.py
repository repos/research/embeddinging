import json
from collections.abc import Iterator
from dataclasses import dataclass
from typing import TextIO


@dataclass(frozen=True)
class Record:
    id: int
    vector: list[float]


@dataclass(frozen=True)
class Query:
    vector: list[float]
    expected_results: list[int]
    expected_scores: list[float]


def read_records(stream: TextIO) -> Iterator[Record]:
    for id, line in enumerate(stream):
        _, embedding = line.strip().split("\t")
        vector = [float(value) for value in embedding.split()]
        yield Record(id=id, vector=vector)


def read_queries(stream: TextIO) -> Iterator[Query]:
    for line in stream:
        query = json.loads(line)
        yield Query(
            vector=query["vector"],
            expected_results=query["expected_results"],
            expected_scores=query["expected_scores"],
        )
