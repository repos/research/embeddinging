from collections.abc import Iterable
from dataclasses import dataclass
from datetime import timedelta
from typing import Any, Protocol

from benchmarks.reader import Query, Record


@dataclass(frozen=True)
class InsertStats:
    insert_time: timedelta
    processing_time: timedelta
    total_time: timedelta


@dataclass(frozen=True)
class SearchStats:
    mean_time: timedelta
    min_time: timedelta
    max_time: timedelta
    p95_time: timedelta
    p99_time: timedelta
    std_time: timedelta
    total_time: timedelta
    requests_per_second: float
    mean_precision: float


class Client(Protocol):
    """Interface for clients to use for benchmarking."""

    def insert(
        self, records: Iterable[Record], insert_params: dict[str, Any]
    ) -> InsertStats:
        """Insert the given `records` into the index.

        Returns collective statistics for all insertions as
        measured for the current index and client configurations.
        """
        ...

    def search(
        self, queries: Iterable[Query], search_params: dict[str, Any]
    ) -> SearchStats:
        """Search the given `records` in the index.

        Returns collective statistics for all searches as
        measured for the current index and client configurations.
        """
        ...
