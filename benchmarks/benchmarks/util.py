import random
from collections.abc import Iterable, Iterator
from itertools import islice
from typing import TypeVar

T = TypeVar("T")


def sample(population: Iterable[T], size: int, seed: int = 42) -> list[T]:
    """Chooses k unique random elements from a population of unknown size.

    Returns a new list containing elements from the population. The length of
    the list equals `size` unless the iterable contains insufficient elements, in which
    case it equals the number of elements available.
    """
    random.seed(seed)

    population = iter(population)
    selected = list(islice(population, size))
    random.shuffle(selected)

    for idx, value in enumerate(population, start=size):
        r = random.randint(0, idx)
        if r < size:
            # Replace random elements in the reservoir with
            # decreasing probability
            selected[r] = value

    return selected


def batched(iterable: Iterable[T], batch_size: int) -> Iterator[tuple[T, ...]]:
    """Batch data into tuples of length n. The last batch may be shorter than n.

    Returns an iterator by looping over the input iterable and accumulating
    data into tuples up to size n. The input is consumed lazily, just enough
    to fill a batch. The result is yielded as soon as a batch is full
    or when the input iterable is exhausted.
    """
    iterable = iter(iterable)
    while batch := tuple(islice(iterable, batch_size)):
        yield batch
