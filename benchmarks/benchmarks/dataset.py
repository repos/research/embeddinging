"""Create a dataset to run benchmarks against.

Since most existing use cases use annoy for approximate nearest
neighbors search, the idea is to create a new dataset for benchmarking
search queries by building an annoy index for an existing vector dataset,
where the queries would be random points from the vector dataset and the
results would be the ones returned by annoy.
"""
import argparse
import gzip
import json
import logging
import sys
from collections.abc import Iterable, Iterator
from dataclasses import asdict
from pathlib import Path

from annoy import AnnoyIndex
from tqdm import tqdm

from benchmarks.reader import Query, Record, read_records
from benchmarks.util import sample


def build_index(records: Iterable[Record], vector_size: int) -> AnnoyIndex:
    """Build an ANN index from `records`.

    Returns a tree-based Annoy index with 25 trees of vectors in records,
    where each vector has dimensions `vector_size`. The distance metric used
    for the index is cosine similarity.
    """
    index = AnnoyIndex(vector_size, metric="angular")
    index.set_seed(42)
    for record in tqdm(records):
        index.add_item(record.id, record.vector)
    index.build(n_trees=25)
    return index


def make_queries(
    records: Iterable[Record], index: AnnoyIndex, results_per_query: int = 3
) -> Iterator[Query]:
    """Make queries from records by searching the index.

    Returns an iterator of `Query` where each query is derived from
    doing an approximate nearest neighbor search in the index and getting
    the top n results and distances when n equals `results_per_query`.
    """
    for record in tqdm(records):
        results, distances = index.get_nns_by_vector(
            vector=record.vector,
            n=results_per_query,
            include_distances=True,
            search_k=1000,
        )
        yield Query(
            vector=record.vector,
            expected_results=results,
            expected_scores=distances,
        )


def generate_dataset(
    embeddings_path: Path, max_rows: int, max_results_per_query: int, seed: int
) -> Iterator[Query]:
    """Generate a queries dataset from embeddings.

    Returns an iterator of queries by sampling `max_rows` records from the
    embeddings dataset and querying an index created from the embeddings for
    each record.
    """
    with gzip.open(embeddings_path, "rt") as record_file:
        first_record = next(read_records(record_file))
        if first_record is None:
            logging.error(
                f"Couldn't read embeddings from file: {embeddings_path}",
            )
            sys.exit(1)

        record_file.seek(0)

        logging.info("Building index from embeddings...")
        vector_size = len(first_record.vector)
        index = build_index(
            records=read_records(record_file),
            vector_size=vector_size,
        )

        record_file.seek(0)

        logging.info(f"Sampling {max_rows} records...")
        sampled_records = sample(
            population=read_records(record_file),
            size=max_rows,
            seed=seed,
        )

    logging.info(
        "Making queries for records with "
        f"{max_results_per_query} results per query..."
    )
    yield from make_queries(
        records=sampled_records,
        index=index,
        results_per_query=max_results_per_query,
    )

    # Might potentially help with debugging
    index.save("annoy_dataset_index.ann")


def main() -> None:
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(
        description=(
            "Create a dataset of queries by randomly sampling "
            "a given embeddings dataset"
        )
    )

    parser.add_argument(
        "--embeddings",
        type=Path,
        required=True,
        help="Path to the trained embeddings",
    )
    parser.add_argument(
        "--output",
        type=Path,
        required=True,
        help="Path to write the queries dataset in jsonl",
    )
    parser.add_argument(
        "--rows",
        type=int,
        required=True,
        help="Number of rows in the output dataset",
    )
    parser.add_argument(
        "--seed",
        type=int,
        required=False,
        default=42,
        help="Seed for randomly sampling embeddings",
    )
    parser.add_argument(
        "--results-per-query",
        type=int,
        required=False,
        default=3,
        help="Number of points to retrieve for every query",
    )
    args = parser.parse_args()

    queries = generate_dataset(
        embeddings_path=args.embeddings,
        max_rows=args.rows,
        max_results_per_query=args.results_per_query,
        seed=args.seed,
    )

    with open(args.output, "w") as file:
        for query in tqdm(queries):
            json.dump(asdict(query), file)
            file.write("\n")

    logging.info(f"Queries written to {args.output}")


if __name__ == "__main__":
    main()
