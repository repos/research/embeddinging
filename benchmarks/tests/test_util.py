from collections.abc import Iterable
from typing import TypeVar

import pytest

from benchmarks.util import batched, sample

T = TypeVar("T")


@pytest.mark.parametrize(
    ("population", "sample_size", "seed", "expected"),
    (
        ([1, 2, 3, 4, 5], 3, 123, [4, 2, 1]),
        ([1, 2, 3, 4, 5], 5, 123, [4, 2, 5, 3, 1]),
        ([1, 2, 3, 4, 5], 6, 123, [4, 2, 5, 3, 1]),
        (range(1, 6), 3, 123, [4, 2, 1]),
        (range(1, 6), 5, 123, [4, 2, 5, 3, 1]),
        (range(1, 6), 6, 123, [4, 2, 5, 3, 1]),
        ((lambda: (yield from range(1, 6)))(), 3, 123, [4, 2, 1]),
        ((lambda: (yield from range(1, 6)))(), 5, 123, [4, 2, 5, 3, 1]),
        ((lambda: (yield from range(1, 6)))(), 6, 123, [4, 2, 5, 3, 1]),
    ),
)
def test_sample(
    population: Iterable[T], sample_size: int, seed: int, expected: list[T]
) -> None:
    assert sample(population, sample_size, seed) == expected


@pytest.mark.parametrize(
    ("iterable", "batch_size", "expected"),
    (
        ([1, 2, 3, 4, 5, 6], 3, [(1, 2, 3), (4, 5, 6)]),
        (range(1, 7), 3, [(1, 2, 3), (4, 5, 6)]),
        ((lambda: (yield from range(1, 7)))(), 3, [(1, 2, 3), (4, 5, 6)]),
        ([1, 2, 3, 4, 5, 6], 4, [(1, 2, 3, 4), (5, 6)]),
        ("ABCDEFG", 2, [("A", "B"), ("C", "D"), ("E", "F"), ("G",)]),
    ),
)
def test_batched(
    iterable: Iterable[T], batch_size: int, expected: list[tuple[T, ...]]
) -> None:
    assert list(batched(iterable, batch_size)) == expected
