# Vector Database Benchmarks
An extensible harness for benchmarking vector databases.

## Generate a test dataset
To generate a dataset of queries from a collection of embeddings for benchmarking, run:
```bash
python3 -m benchmarks.dataset \
  --embeddings 'embeddings.tsv.gz' \
  --output 'queries.jsonl' \
  --rows 10000 \
  --seed 42 \
  --results-per-query 5
```

## Run a benchmark
Running any benchmark for a database requires choosing a server configuration, a dataset and an experiment, which is a specific client configuration like number of instances and search parameters etc.

### Launch the server
Launch the server as a Skein application:
```bash
python3 -m benchmarks.server launch benchmarks/servers/<server-spec>.yaml
```

List all addresses the the server is listening on:
```bash
python3 -m benchmarks.server hosts <application-id>
```

### Run the experiment
Run an experiment against the launched server:
```bash
python3 -m benchmarks.experiment \
  --name <experiment-name> \
  --dataset <dataset-path> \
  --output <result-path> \
  --server 'http://<server-address>'
```
