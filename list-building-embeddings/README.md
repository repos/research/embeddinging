## Uploading embeddings
To upload a set of embeddings to a new or existing collection in a running qdrant instance:

```bash
python3 -m list_building_embeddings upload \
  --database-url 'https://vector-search.wmcloud.org' \
  --embeddings 'embeddings.tsv.gz' \
  --collection-name 'test_collection' \
  --workers 4
```

## Creating a snapshot of embeddings
Snapshots allow taking a collection of embeddings from one qdrant instance and replicating it on another. This can be useful for:
* Archiving collections.
* Creating collections on machines with a large number of resources, like the stat boxes, so that creating, indexing and optimizing collections is fast and then recovering these collections on smaller instances for serving.

To create a snapshot for a new or existing collection in a running qdrant instance:

```bash
python3 -m list_building_embeddings snaphot \
  --database-url 'http://localhost:6333' \
  --embeddings 'embeddings.tsv.gz' \
  --collection-name 'test_collection' \
  --output 'hdfs:///temp/list_building'
```

## Creating regular snapshots
Regular snapshots can be generated for frequently updated embeddings using Airflow. An example of a dag for this purpose can be found under `airflow`.
