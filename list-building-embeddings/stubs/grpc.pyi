from enum import Enum

class StatusCode(Enum):
    NOT_FOUND = "not found"

class RpcError(Exception):
    def code(self) -> StatusCode: ...
