import io
from typing import Any

class AbstractBufferedFile(io.IOBase): ...

class OpenFile:
    def __enter__(self) -> AbstractBufferedFile: ...
    def __exit__(self, *args: Any) -> None: ...

def open(  # noqa: PLR0913
    urlpath: str,
    mode: str = "rb",
    compression: str | None = None,
    encoding: str = "utf8",
    errors: str | None = None,
    protocol: str | None = None,
    newline: bytes | None = None,
    **kwargs: Any,
) -> OpenFile: ...
