import logging

import typer
from rich.logging import RichHandler

from list_building_embeddings.embeddings import app as embeddings_app
from list_building_embeddings.server import app as server_app

app = typer.Typer(rich_markup_mode="rich")
app.add_typer(
    embeddings_app,
    name="embeddings",
    help="Manage existing and new collections of embeddings",
)
app.add_typer(
    server_app,
    name="server",
    help="Launch a vector database",
)


def entry_point() -> None:
    """Run the typer cli application. This function serves as an
    entry point to this application, allowing typer to parse arguments
    before passing them to commands. Use this if you're defining
    a console script for this module.
    """
    logging.basicConfig(
        level=logging.INFO,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler()],
    )
    app()


if __name__ == "__main__":
    entry_point()
