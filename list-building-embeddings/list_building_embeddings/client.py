import logging
import os
import time
from dataclasses import dataclass, field
from pathlib import Path
from typing import Final, TextIO, cast

import fsspec
import grpc
import requests
import yaml
from qdrant_client import QdrantClient
from qdrant_client.http import models as qmodels
from tqdm import tqdm
from typing_extensions import Self

from list_building_embeddings.record import read_records


@dataclass(frozen=True)
class CollectionConfig:
    hnsw_index: qmodels.HnswConfigDiff = field(
        default_factory=lambda: qmodels.HnswConfigDiff(
            m=32, ef_construct=256, on_disk=False
        )
    )
    optimizers: qmodels.OptimizersConfigDiff = field(
        default_factory=lambda: qmodels.OptimizersConfigDiff(
            memmap_threshold=20 * 1024, default_segment_number=4
        )
    )

    @classmethod
    def from_yaml(cls, path: Path) -> Self:
        config = yaml.safe_load(path.read_text())
        return cls(
            hnsw_index=qmodels.HnswConfigDiff(**config.get("hnsw_index", {})),
            optimizers=qmodels.OptimizersConfigDiff(**config.get("optimizers", {})),
        )


DEFAULT_COLLECTION_CONFIG: Final[CollectionConfig] = CollectionConfig()


class DatabaseClient:
    """Entry point for communicating with the vector database"""

    def __init__(self, database_url: str) -> None:
        self._url = database_url
        self._client = QdrantClient(url=self._url, prefer_grpc=True, timeout=360)

    def _is_collection_optimized(self, collection_name: str) -> bool:
        collection_info = self._client.get_collection(collection_name)
        if collection_info.status == qmodels.CollectionStatus.GREEN:
            # Sometimes collections are momentarily reported as green even
            # though they are not done optimizing so we need to wait some more
            # to make sure the collections stays green.
            time.sleep(5.0)

            collection_info = self._client.get_collection(collection_name)
            if collection_info.status == qmodels.CollectionStatus.GREEN:
                return True

        return False

    def upload(
        self,
        collection_name: str,
        embeddings_path: str,
        workers: int,
        collection_config: CollectionConfig | None = None,
    ) -> None:
        """Batch upload vectors to a collection.

        If a collection with `collection_name` does not exist,
        it will be created with `collection_config` prior to upload.
        If `collection_config` is `None`, a default config will be applied.
        """
        with fsspec.open(embeddings_path, mode="rt", compression="gzip") as file:
            first_record = next(read_records(cast(TextIO, file)))
            if first_record is None:
                raise ValueError(f"Couldn't read embeddings from: {file}")

            self.create_collection(
                collection_name=collection_name,
                vector_size=len(first_record.vector),
                collection_config=collection_config,
            )
        self._client.update_collection(
            collection_name=collection_name,
            optimizer_config=qmodels.OptimizersConfigDiff(
                max_optimization_threads=0,
            ),
        )

        with fsspec.open(embeddings_path, mode="rt", compression="gzip") as file:
            collection_records = (
                qmodels.Record(
                    id=record.id,
                    vector=record.vector,
                    payload={"name": record.name},
                )
                for record in read_records(cast(TextIO, file))
            )

            logging.info(
                "Uploading records to %s using %d workers...", collection_name, workers
            )
            self._client.upload_records(
                collection_name=collection_name,
                records=tqdm(
                    collection_records,
                    desc="Records uploaded",
                    unit="",
                ),
                parallel=workers,
            )

        self._client.update_collection(
            collection_name=collection_name,
            optimizer_config=qmodels.OptimizersConfigDiff(
                max_optimization_threads=1,
            ),
        )

        logging.info("Optimizing collection %s...", collection_name)

        while True:
            time.sleep(1.0)
            if self._is_collection_optimized(collection_name):
                return

    def create_collection(
        self,
        collection_name: str,
        vector_size: int,
        collection_config: CollectionConfig | None,
    ) -> None:
        """Create a collection, if it does not already exist."""

        if collection_config is None:
            collection_config = DEFAULT_COLLECTION_CONFIG

        vectors_config = qmodels.VectorParams(
            size=vector_size,
            distance=qmodels.Distance.COSINE,
            on_disk=True,
        )
        try:
            logging.info("Checking if collection %s exists...", collection_name)
            self._client.get_collection(collection_name)
            logging.info("Found collection %s in database.", collection_name)
        except grpc.RpcError as err:
            if err.code() == grpc.StatusCode.NOT_FOUND:
                logging.info(
                    "Collection %s doesn't exist. Creating it with config: %r...",
                    collection_name,
                    collection_config,
                )
                self._client.create_collection(
                    collection_name=collection_name,
                    vectors_config=vectors_config,
                    optimizers_config=collection_config.optimizers,
                    hnsw_config=collection_config.hnsw_index,
                )
            else:
                raise

    def create_snapshot(
        self,
        collection_name: str,
        embeddings_path: str | None,
        output_dir: str,
        workers: int,
        collection_config: CollectionConfig | None = None,
    ) -> str:
        """Create a snapshot of a collection and save it to disk.

        If a collection with `collection_name` does not exist, it will be created.
        Vectors in the `embeddings` file will be uploaded to the collection prior to
        creating the snapshot.
        """
        if embeddings_path:
            self.upload(
                collection_name=collection_name,
                collection_config=collection_config,
                embeddings_path=embeddings_path,
                workers=workers,
            )

        logging.info("Creating index for %s...", collection_name)
        self._client.create_payload_index(
            collection_name=collection_name,
            field_name="name",
            field_schema=qmodels.PayloadSchemaType.KEYWORD,
        )

        logging.info("Creating snapshot for %s...", collection_name)
        snapshot = self._client.create_snapshot(collection_name)
        if snapshot is None:
            raise RuntimeError(f"Couldn't create snapshot for {collection_name}")

        snapshot_path = os.path.join(output_dir, snapshot.name)
        collection_url = f"{self._url}/collections/{collection_name}"
        snapshot_url = f"{collection_url}/snapshots/{snapshot.name}"

        logging.info("Saving snapshot to %s...", snapshot_path)
        with requests.get(snapshot_url, stream=True) as resp:
            resp.raise_for_status()
            with fsspec.open(snapshot_path, mode="wb") as snapshot_file:
                for chunk in tqdm(
                    resp.iter_content(chunk_size=8192),
                    desc="Snapshot downloaded",
                    unit_scale=8192,
                    unit=" bytes",
                ):
                    snapshot_file.write(chunk)

        return snapshot_path
