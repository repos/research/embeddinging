import logging
import stat
import subprocess
from pathlib import Path
from typing import Final, Optional

import requests
import rich

# TODO: add stubs for skein
import skein  # type: ignore[import-untyped]
import tqdm
import typer

from list_building_embeddings import yarn
from list_building_embeddings.utils import Url

app: Final[typer.Typer] = typer.Typer(subcommand_metavar="MODE")


class VectorDatabase:
    """Database for storing and searching embeddings."""

    DEFAULT_EXECUTABLE_URL: Final[Url] = Url(
        "https://gitlab.wikimedia.org/repos/research/embeddinging/-/jobs/artifacts/main/raw/list-building-embeddings/qdrant/target/release/qdrant?job=build-qdrant"
    )
    DEFAULT_EXECUTABLE_PATH: Final[Path] = (
        Path.home() / ".cache" / "list_building_embeddings" / "qdrant"
    )

    def __init__(
        self,
        http_port: int,
        workers: int,
        executable_url: Url = DEFAULT_EXECUTABLE_URL,
        executable_path: Path = DEFAULT_EXECUTABLE_PATH,
    ) -> None:
        self.http_port = http_port
        self.workers = workers
        self.executable_url = executable_url
        self.executable_path = executable_path

        self.download_executable()

    def download_executable(self) -> Path:
        executable_name = self.executable_path.name
        if self.executable_path.exists():
            logging.info("Found %s at: %s", executable_name, self.executable_path)
            return self.executable_path

        logging.info(
            "Executable not found at %s. Downloading from: %s",
            self.executable_path,
            self.executable_url,
        )
        self.executable_path.parent.mkdir(exist_ok=True, parents=True)
        with requests.get(self.executable_url, stream=True) as resp:
            resp.raise_for_status()
            with open(self.executable_path, mode="wb") as executable_file:
                for chunk in tqdm.tqdm(
                    resp.iter_content(chunk_size=8192),
                    desc="Executable downloaded",
                    unit_scale=True,
                    unit=" bytes",
                ):
                    executable_file.write(chunk)

        # Set the execute bit on the downloaded file
        mode = self.executable_path.stat().st_mode | stat.S_IEXEC
        self.executable_path.chmod(mode)
        return self.executable_path

    def launch_local(self) -> tuple[subprocess.Popen[str], Url]:
        """Launch a vector database listening on localhost."""

        logging.info(
            "Starting database with %d workers on: \nhttp://127.0.0.1:%d",
            self.workers,
            self.http_port,
        )
        proc = subprocess.Popen(
            args=[f"{self.executable_path}"],
            env={
                "QDRANT__SERVICE__HTTP_PORT": f"{self.http_port}",
                "QDRANT__SERVICE__MAX_WORKERS": f"{self.workers}",
            },
            text=True,
        )
        return (proc, Url(f"http://127.0.0.1:{self.http_port}"))

    def launch_on_yarn(self, memory_mb: int, cores: int) -> yarn.ServiceInfo:
        """Launch a vector database with specified resources on yarn."""

        db_service = skein.Service(
            files={"qdrant": str(self.executable_path)},
            resources=skein.Resources(vcores=cores, memory=memory_mb),
            script="./qdrant",
            env={
                "QDRANT__SERVICE__HTTP_PORT": str(self.http_port),
                "QDRANT__SERVICE__MAX_WORKERS": str(self.workers),
            },
        )
        return yarn.submit_service(
            service_name="qdrant-single-node",
            service_description=db_service,
        )


@app.command(name="local", rich_help_panel="Mode")
def launch_local(
    executable: Path = typer.Option(
        envvar="PATH_TO_DB",
        default=VectorDatabase.DEFAULT_EXECUTABLE_PATH,
        help="Path to the database executable",
    ),
    http_port: int = typer.Option(
        default=6333, help="Port at which server listens for HTTP requests"
    ),
    workers: int = typer.Option(
        default=8, help="Number of parallel workers for serving the database"
    ),
    detached: bool = typer.Option(
        default=False, help="Run the database server in the background"
    ),
) -> None:
    """Launch a single node vector database listening on localhost."""

    vector_database = VectorDatabase(
        executable_path=executable,
        http_port=http_port,
        workers=workers,
    )
    proc, _ = vector_database.launch_local()

    if detached:
        rich.print(
            f"""Started server in the background with PID: {proc.pid}
To kill the server run:
[bold green]$ kill -TERM {proc.pid}
"""
        )
    else:
        with proc:
            try:
                # Read data from stdout until EOF and print it to
                # the terminal, waiting for the process to terminate.
                proc.communicate()
            except KeyboardInterrupt:
                # If SIGINT is received, kill the server and
                # let the interrupt bubble up.
                proc.kill()
                raise


@app.command(name="yarn", rich_help_panel="Mode")
def launch_on_yarn(
    executable: Path = typer.Option(
        envvar="PATH_TO_DB",
        default=VectorDatabase.DEFAULT_EXECUTABLE_PATH,
        help="Path to the database executable",
    ),
    http_port: int = typer.Option(
        default=6333,
        help="Port at which server listens for HTTP requests",
    ),
    workers: int = typer.Option(
        default=8,
        help="Number of parallel workers used for serving the database",
    ),
    memory: int = typer.Option(
        default=16 * 1024,
        help="Amount of RAM in MiB to allocate to the instance",
    ),
    cores: Optional[int] = typer.Option(  # noqa: UP007  X | Y unsupported
        min=1,
        default=None,
        show_default=False,
        help=(
            "Number of cores to allocate to the instance; "
            "equal to the number of workers if not set"
        ),
    ),
) -> None:
    """Launch a single node vector database on yarn."""

    vector_database = VectorDatabase(
        executable_path=executable,
        http_port=http_port,
        workers=workers,
    )
    yarn_db = vector_database.launch_on_yarn(
        memory_mb=memory,
        cores=(workers if cores is None else cores),
    )
    rich.print(
        f"""
Vector database with {workers} workers running on:
[bold cyan]{yarn_db.address}:{http_port}[/bold cyan]
To view logs, run:
[bold green]$ yarn logs -applicationId {yarn_db.application_id}[/bold green]
To kill server, run:
[bold green]$ yarn application -kill {yarn_db.application_id}[/bold green]
"""
    )
