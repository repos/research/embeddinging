from dataclasses import dataclass, field
from pathlib import Path
from typing import Final, Optional

import rich
import typer

from list_building_embeddings.client import (
    DEFAULT_COLLECTION_CONFIG,
    CollectionConfig,
    DatabaseClient,
)

app = typer.Typer()


@dataclass(frozen=True)
class CommonOptions:
    database_url: str = field(
        default_factory=lambda: typer.Option(
            show_default=False,
            help="Url of the vector database",
        )
    )
    collection_name: str = field(
        default_factory=lambda: typer.Option(
            show_default=False,
            help="Collection to upload vectors to",
        )
    )
    embeddings: str = field(
        default_factory=lambda: typer.Option(
            show_default=False,
            help=(
                "Absolute path to the embeddings gzipped file in tsv format. "
                "Supported types include local and hdfs files. "
                "If passing a non-local file, also specify the protocol"
            ),
        )
    )
    workers: int = field(
        default_factory=lambda: typer.Option(
            help=(
                "Number of parallel workers to use for uploading vectors. "
                "More workers mean faster upload but also higher load on the database."
            ),
            default=8,
        )
    )
    config: Path = field(
        default_factory=lambda: typer.Option(
            default=None,
            show_default=False,
            help=f"""
Path to yaml collection config i.e. the optimizers and hnsw index config in the qdrant
[link=https://github.com/qdrant/qdrant/blob/master/config/config.yaml]config file[/link].
Omit if you want the default config or your collection already exists.


The default config overrides the following based on previous experimentation:


[underline]hnsw_index[/underline]:

    [bold purple]m[/bold purple]: Number of edges per node in the index graph\n
    [white]Larger the value - more accurate the search, more space required[/white]\n
    [bold]value[/bold]: {DEFAULT_COLLECTION_CONFIG.hnsw_index.m}

    [bold purple]ef_construct[/bold purple]: Number of neighbours to consider during the index building\n
    [white]Larger the value - more accurate the search, more time required to build index[/white]\n
    [bold]value[/bold]: {DEFAULT_COLLECTION_CONFIG.hnsw_index.ef_construct}

    [bold purple]on_disk[/bold purple]: Store HNSW index on disk\n
    [white]If set to false, index will be stored in RAM[/white]\n
    [bold]value[/bold]: {DEFAULT_COLLECTION_CONFIG.hnsw_index.on_disk}

[underline]optimizers[/underline]:

    [bold purple]memmap_threshold[/bold purple]: Maximum size (KiB) of vectors to store in-memory per segment\n
    [white]Segments larger than this threshold will be stored as read-only memmaped file[/white]\n
    [bold]value[/bold]: {DEFAULT_COLLECTION_CONFIG.optimizers.memmap_threshold}

    [bold purple]default_segment_number[/bold purple]: Number of collection segments\n
    [white](Set this to number of cores on server for optimal latency)[/white]\n
    [bold]value[/bold]: {DEFAULT_COLLECTION_CONFIG.optimizers.default_segment_number}

""",
        )
    )


OPTIONS: Final[CommonOptions] = CommonOptions()


@app.command()
def upload(
    database_url: str = OPTIONS.database_url,
    collection_name: str = OPTIONS.collection_name,
    embeddings: str = OPTIONS.embeddings,
    config: Optional[Path] = OPTIONS.config,  # noqa: UP007  X | Y is unsupported
    workers: int = OPTIONS.workers,
) -> None:
    """Batch upload vectors to a collection."""

    client = DatabaseClient(database_url)
    client.upload(
        collection_name=collection_name,
        embeddings_path=embeddings,
        workers=workers,
        collection_config=(
            DEFAULT_COLLECTION_CONFIG
            if config is None
            else CollectionConfig.from_yaml(path=config)
        ),
    )


@app.command()
def snapshot(
    database_url: str = OPTIONS.database_url,
    collection_name: str = OPTIONS.collection_name,
    embeddings: str = OPTIONS.embeddings,
    config: Optional[Path] = OPTIONS.config,  # noqa: UP007  X | Y is unsupported
    workers: int = OPTIONS.workers,
    output: str = typer.Option(
        help="Directory to write the snapshot file to",
        show_default=False,
    ),
) -> None:
    """Create a snapshot of a collection and save it to disk."""

    client = DatabaseClient(database_url)
    snapshot_path = client.create_snapshot(
        collection_name=collection_name,
        embeddings_path=embeddings,
        output_dir=output,
        workers=workers,
        collection_config=(
            DEFAULT_COLLECTION_CONFIG
            if config is None
            else CollectionConfig.from_yaml(path=config)
        ),
    )
    rich.print(f"Saved snapshot to: \n[purple]{snapshot_path}[/purple]")
