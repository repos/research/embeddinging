from collections.abc import Iterator
from dataclasses import dataclass
from typing import TextIO


@dataclass(frozen=True)
class Record:
    id: int
    name: str
    vector: list[float]


def read_records(stream: TextIO) -> Iterator[Record]:
    """Lazily read records from an embeddings stream.

    Embeddings are expected to be delimited by newline, with
    each line containing the name of the entity and the embedding
    of the entity, separated by tab (\t).
    """
    for id, line in enumerate(stream):
        name, embedding = line.strip().split("\t")
        vector = [float(value) for value in embedding.split()]
        yield Record(id=id, name=name, vector=vector)
