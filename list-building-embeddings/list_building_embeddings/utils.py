from typing import NewType

Url = NewType("Url", str)
