import logging
import time
from dataclasses import dataclass

# TODO: add stubs for skein
import skein  # type: ignore[import-untyped]

from list_building_embeddings.utils import Url


@dataclass(frozen=True)
class ServiceInfo:
    application_id: str
    address: Url


def submit_service(
    service_name: str,
    service_description: skein.Service,
) -> ServiceInfo:
    spec = skein.ApplicationSpec(
        name=service_name,
        services={service_name: service_description},
    )
    logging.info("Submitting skein application with spec: %r", spec.to_dict())

    with skein.Client() as client:
        app_id = client.submit(spec)
        app = client.connect(app_id)
        [db_container] = app.get_containers(services=(service_name,))

        logging.info("Waiting for service to run. This might take a few seconds...")
        while db_container.state != "RUNNING":
            time.sleep(1)
            [db_container] = app.get_containers(services=(service_name,))
        db_address, *_ = db_container.yarn_node_http_address.partition(":")

        return ServiceInfo(
            application_id=app_id,
            address=Url(db_address),
        )
