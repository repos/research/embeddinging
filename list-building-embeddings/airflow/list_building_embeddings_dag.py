import json
import shlex
from datetime import datetime, timedelta

from pydantic import EmailStr
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder
from research.config import dag_config
from typing_extensions import Self
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

from airflow import DAG
from airflow.models import Variable


@dataclass(frozen=True)
class DagProperties:
    # Dag configuration
    alerts_email: EmailStr = "research-engineering-alerts@lists.wikimedia.org"
    conda_env: str = "/home/mnz/list-building-embeddings.tar.gz"
    sla: timedelta = timedelta(days=3)
    # Application arguments
    embeddings_path: str = (
        # Scheme needs to be specified with the file path
        # since fsspec relies on it to infer filesystem.
        f"{dag_config.hadoop_name_node}{dag_config.hdfs_temp_directory}"
        "/list_building"
        "/embeddings"
        "/model_{{ data_interval_start | to_ds_month }}.tsv.gz"
    )
    snapshot_dir: str = (
        # Scheme needs to be specified with the file path
        # since fsspec relies on it to infer filesystem.
        f"{dag_config.hadoop_name_node}{dag_config.hdfs_temp_directory}"
        "/list_building"
        "/snapshots"
        "/{{ data_interval_start | to_ds_month }}"
    )
    collection_name: str = "list-building"

    @classmethod
    def from_variable(cls, variable_key: str) -> Self:
        """Load properties from the airflow variable associated with `variable_key`.
        If no value is found for the given key, the variable will be initialized
        with the defaults in `DagProperties`.
        """
        # Get the current value of the variable or initialize with
        # an empty dict and store that as the value in json
        variable_value = Variable.setdefault(variable_key, default=dict(), deserialize_json=True)

        # Initialize DagProperties and override properties found in value
        dag_properties = cls(**variable_value)

        # Set dag_properties as the new variable value after serializing it to json.
        dag_properties_json = json.dumps(dag_properties, default=pydantic_encoder, indent=4)
        Variable.update(key=variable_key, value=dag_properties_json)

        return dag_properties


props = DagProperties.from_variable("list_building_embeddings")
default_args = dag_config.default_args | {"email": props.alerts_email, "sla": props.sla}

skein_memory_mb = 32000
skein_vcores = 16


with DAG(
    dag_id="list_building_embeddings",
    schedule="@monthly",
    start_date=datetime(2023, 10, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=("research", "list-building"),
    catchup=False,
) as dag:
    wait_for_embeddings = URLSensor(
        task_id="wait_for_embeddings",
        url=str(props.embeddings_path),
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    list_building_embeddings_args = {
        "--database-url": "http://localhost:6333",
        "--collection-name": props.collection_name,
        "--embeddings": props.embeddings_path,
        "--output": str(props.snapshot_dir),
    }
    list_building_embeddings_args_str = " ".join(
        f"{option} {shlex.quote(value)}"
        for option, value in list_building_embeddings_args.items()
    )

    create_snapshot = SimpleSkeinOperator(
        task_id="create_snapshot",
        files={"env": props.conda_env},
        resources={"memory": skein_memory_mb, "vcores": skein_vcores},
        script=" && ".join(
            [
                "source env/bin/activate",
                "(QDRANT__SERVICE__MAX_WORKERS=8 qdrant > qdrant.log 2>&1 &)",
                "export CLASSPATH=$($HADOOP_HOME/bin/hadoop classpath --glob)",
                f"python3 -m list_building_embeddings snapshot {list_building_embeddings_args_str}",
            ]
        ),
    )

    wait_for_embeddings >> create_snapshot
