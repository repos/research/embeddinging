import gzip
import signal
import subprocess
import sys
import tempfile
import time
from collections.abc import Generator
from itertools import islice
from pathlib import Path
from typing import TypeAlias
from unittest import mock

import pytest
from typer.testing import CliRunner as TyperCliRunner

from list_building_embeddings import yarn
from list_building_embeddings.__main__ import app
from list_building_embeddings.utils import Url

DatabaseInstance: TypeAlias = tuple[subprocess.Popen[str], Url]


@pytest.fixture(scope="session")
def cli_runner() -> TyperCliRunner:
    return TyperCliRunner()


@pytest.fixture(scope="function")
def local_vector_database() -> Generator[DatabaseInstance, None, None]:
    with tempfile.TemporaryDirectory() as tmp_dir:
        proc = subprocess.Popen(
            (
                sys.executable,
                "-m",
                "list_building_embeddings",
                "server",
                "local",
                "--http-port",
                "8899",
                "--workers",
                "2",
            ),
            cwd=tmp_dir,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        # We need to wait until the server's up before allowing tests
        # to connect to avoid getting 'refused to connect' errors.
        time.sleep(3)

        yield (proc, Url("http://127.0.0.1:8899"))

        # Tear down the database
        proc.send_signal(signal.SIGINT)
        proc.wait()


@pytest.fixture(scope="session")
def example_embeddings() -> Generator[Path, None, None]:
    embeddings = b"""Q1	0.1 0.6 0.3 0.8 0.4
Q2	0.5 0.3 0.4 0.2 0.1
Q3	0.4 0.3 0.4 0.1 0.7
"""
    with tempfile.NamedTemporaryFile(mode="wt") as file:
        with gzip.GzipFile(filename=file.name, mode="wb") as gzip_file:
            gzip_file.write(embeddings)
        yield Path(file.name)


def test_help(cli_runner: TyperCliRunner) -> None:
    result = cli_runner.invoke(app, ("--help",))
    assert result.exit_code == 0
    assert "Usage" in result.stdout


def test_server_local(local_vector_database: DatabaseInstance) -> None:
    proc, _ = local_vector_database
    assert proc.stdout is not None

    output = "".join(islice(proc.stdout, 10))
    assert "Starting database with 2 workers on:" in output
    assert "http://127.0.0.1:8899" in output


def test_server_yarn(cli_runner: TyperCliRunner) -> None:
    mock_service_info = yarn.ServiceInfo(
        application_id="mockid123",
        address=Url("127.0.0.1"),
    )
    with mock.patch.object(yarn, "submit_service", return_value=mock_service_info):
        result = cli_runner.invoke(
            app,
            (
                "server",
                "yarn",
                "--http-port",
                "6868",
                "--workers",
                "3",
                "--memory",
                "4096",
                "--cores",
                "3",
            ),
        )
        assert result.exit_code == 0

        expected_output = f"""
Vector database with 3 workers running on:
{mock_service_info.address}:6868
To view logs, run:
$ yarn logs -applicationId {mock_service_info.application_id}
To kill server, run:
$ yarn application -kill {mock_service_info.application_id}
"""
        assert expected_output in result.stdout


def test_embeddings_upload(
    cli_runner: TyperCliRunner,
    local_vector_database: DatabaseInstance,
    example_embeddings: Path,
) -> None:
    _, local_db_url = local_vector_database
    result = cli_runner.invoke(
        app,
        (
            "embeddings",
            "upload",
            "--database-url",
            local_db_url,
            "--collection-name",
            "test-upload",
            "--embeddings",
            str(example_embeddings),
        ),
    )
    assert result.exit_code == 0
    assert "Records uploaded: 3" in result.stdout


def test_embeddings_snapshot(
    cli_runner: TyperCliRunner,
    local_vector_database: DatabaseInstance,
    example_embeddings: Path,
) -> None:
    _, local_db_url = local_vector_database
    with tempfile.TemporaryDirectory() as temp_dir:
        result = cli_runner.invoke(
            app,
            (
                "embeddings",
                "snapshot",
                "--database-url",
                local_db_url,
                "--collection-name",
                "test-snapshot",
                "--embeddings",
                str(example_embeddings),
                "--output",
                temp_dir,
            ),
        )
        assert result.exit_code == 0
        assert f"Saved snapshot to: \n{temp_dir}" in result.stdout
        assert len(list(Path(temp_dir).iterdir())) == 1
